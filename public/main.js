const { app, BrowserWindow } = require('electron')

const path = require('path')
const isDev = require('electron-is-dev')

require('@electron/remote/main').initialize()

app.commandLine.appendSwitch('enable-unsafe-webgpu') // WEB GPU

// function createWindow () {
//     const mainWindow = new BrowserWindow()
//
//     mainWindow.loadURL('https://austineng.github.io/webgpu-samples/?wgsl=0#rotatingCube')
// }
function createWindow() {
    // Create the browser window.
    const win = new BrowserWindow({

        width: 800,
        height: 600,
        title: "Projection Engine",
        protocol: 'file:',
        webPreferences: {

            enableRemoteModule: true,
            nodeIntegration: true,
            contextIsolation: false
        },
        autoHideMenuBar: true,

    })
    win.openDevTools();
    win.loadURL(
        isDev
            ? 'http://localhost:3000'
            : `file://${path.join(__dirname, '../build/index.html')}`
    )
}

app.whenReady().then(createWindow)

// Quit when all windows are closed.
app.on('window-all-closed', function () {
    // On OS X it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') {
        app.quit()
    }
})

app.on('activate', function () {
    // On OS X it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) createWindow()
})