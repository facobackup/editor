import MaterialInstance from "../engine/elements/instances/MaterialInstance";
import EVENTS from "./misc/EVENTS";
import cloneClass from "./misc/cloneClass";

export default function importMaterial(mat, engine, load) {
    const newMat = new MaterialInstance(
        engine.gpu,
        mat.id
    )

    let found = engine.materials.find(m =>m  !== undefined &&  m.id === mat.id)
    if (!found) {
        load.pushEvent(EVENTS.LOADING_MATERIAL)
        newMat.initializeTextures(
            mat.blob.albedo,
            mat.blob.metallic,
            mat.blob.roughness,
            mat.blob.normal,
            mat.blob.height,
            mat.blob.ao,
        ).then(() => {
            engine.setMaterials(prev => {
                return [...prev, newMat]
            })

            load.finishEvent(EVENTS.LOADING_MATERIAL)
        })
    }

}