export default {

    PROJECT_DATA: "Loading project data",
    PROJECT_DELETE: "Deleting project",
    PROJECT_LIST: "Loading projects",
    PROJECT_SAVE: "Saving project",
    PROJECT_FILES: "Project files",
    PROJECT_IMPORT: "Importing project",

    UPDATE_FILE: "Updating file",
    DELETE_FILE: "Deleting file",
    LOAD_FILE: "Loading file",
    MOVE_FILE: "Moving file",
    IMPORT_FILE: "Importing files",


    DELETE_FOLDER: 'Deleting folder',

    SKYBOX: "Skybox",
    MESH: "Mesh",
    MATERIAL: "Material",

    REFRESHING: 'Refreshing files',
    LOADING_VIEWPORT: 'Loading viewport',
    PACKAGING_DATA: 'Packaging data for download.',
    LOADING_MATERIAL: 'Loading material',
    LOAD_FILES: 'Loading assets',
    COMPILING: 'Compiling',
    UPDATING_SYSTEMS: 'Compiling systems',
    LOADING_MESHES: 'Loading meshes',
    SAVING_MESH: 'Saving mesh'
}
